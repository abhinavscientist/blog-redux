import React from 'react';
import { combineReducers } from 'redux';
import PostReducers from './PostReducers';
import UserReducer from './UserReducer';

// ele is action, r1 is a reducer, it receives an element and works on it.
// const r1 = (ele = 0) => {
//     return ele;
// };

// // ele is action
// const r2 = (ele = 0) => {
//     return ele
// };

// const reducers = combineReducers({
//     r1: r1,
//     r2: r2
// });

// Rules
// 1. reducers must return some value other than undefined
// 2. At the start of redux application, each reducer is automatically called one time.
// 3. First call to reducers is called with default value. 
// 4. Second time onwards, reducer is called with State V1 and so on.
// 5. Do not make api call in the reducer, keep it pure. 
// 6. DO NOT mutate state in reducer. 
// In javascript, strings and numbers are immutable unlike Objects and Arrays
const reducers = combineReducers({
    posts: PostReducers, 
    users: UserReducer
});

export default reducers;