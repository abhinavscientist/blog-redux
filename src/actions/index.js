import _ from 'lodash';
import jsonPlaceHolder from '../apis/JsonPlaceHolder';

// function style of writing
// export const fetchPost = function(){
    // dispatch, getState are functions coming from redux store.
    // dispatch and getState is given by redux thunk to inner functions as parameters.
//     return async function(dispatch, getState){
//         const response = await JsonPlaceHolder.get('/post')
//         dispatch({
//             type: 'FETCH_POSTS', 
//             payload: response
//         });
//     }
// };

// function style of writing
// export const fetchUser = function(id) { 
//     return  async function(dispatch, getState){
//             const response = await jsonPlaceHolder.get(`/users/${id}`);
//             dispatch({type: 'FETCH_USER', payload: response.data});
//     };
// }; 

export const fetchPostsAndUsers = () => async (dispatch, getState) => {
    // whenever we call an action creator from inside and action creator, we need 
    // to ensure that we manually dispatch it. 
    console.log('About to fetch posts');
    await dispatch(fetchPost());
    // console.log(getState().posts);
    
    // const userIDArray = _.map(getState().posts, 'userId')
    // const userIds = _.uniq(userIDArray);

    // const userIds = _.uniq(_.map(getState().posts, 'userId'));
    // // async-await syntax does not work with foreach statement.
    // userIds.forEach( (id) => dispatch(fetchUser(id)));

    _.chain(getState().posts)
    .map('userId')
    .uniq()
    .forEach((id) => dispatch(fetchUser(id)))
    .value();
};


// fetchPost is a function and an action creator
export const fetchPost = () => async dispatch => {
        const response = await jsonPlaceHolder.get('/posts');
        dispatch({type: 'FETCH_POSTS', payload: response.data});
    };

export const fetchUser = (id) => async dispatch => {
    const response = await jsonPlaceHolder.get(`/users/${id}`);
    dispatch({type: 'FETCH_USER', payload: response.data});
};    

//memoized version
// export const fetchUser = (id) => dispatch => {
//     _fetchUser(id, dispatch);
        
// };

// const _fetchUser = _.memoize(async (id, dispatch) => {
//     const response = await jsonPlaceHolder.get(`/users/${id}`);
//     dispatch({type: 'FETCH_USER', payload: response.data});
// });


