import React from 'react';
import { connect } from 'react-redux';
import { fetchPost, fetchPostsAndUsers } from '../actions';
import UserHeader from './UserHeader';

// We chose PostList as class type it can provide access to lifecycle methods.
class PostList extends React.Component {

    componentDidMount(){
        // fetchPost is an action creator
        // this.props.fetchPost();
        this.props.fetchPostsAndUsers();
    }

    renderList(){
        return this.props.posts.map(post => {
            return (
                <div className="item" key={post.id}>
                    <i className="Large middle aligned icon user" />
                    <div className="content">
                        <div className="description">
                            <h2>{post.title}</h2>
                            <p>{post.body}</p>
                        </div>
                        <UserHeader userId={post.userId}/>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
        <div className="ui relaxed divided list">{this.renderList()}</div>
        );
    }
}

const mapStateToProps = (state) => {
    return { posts: state.posts}
};

// First Parameter in connect is always mapStateToProps. It is null for the time being.
export default connect(
    mapStateToProps, 
    {fetchPost, fetchPostsAndUsers} // fetchPost is the action creator.
)(PostList);

