import React from 'react';
import Redux from 'redux';
import ReactRedux from 'react-redux';
import PostList from './PostList';

const App = () => {
    return (
        <div className="ui container">
            <PostList />
        </div>
    );
};

export default App;