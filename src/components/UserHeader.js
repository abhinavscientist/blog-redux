import React from 'react';
import { connect } from 'react-redux';
// import { fetchUser } from '../actions';

class UserHeader extends React.Component {

    componentDidMount() {
        // this.props.fetchUser(this.props.userId);
    }

    render(){
        // const user = this.props.user;
        const { user } = this.props;
        
        if (!user) {
            return null;
        }

        return <div className='header'>{user.name}</div>;
    }

}

// state has come from redux provider defined in the top of the list.
const mapStateToProp = (state, ownProps) => {
    return {user: state.users.find(user => user.id === ownProps.userId)}
};

// export default connect(mapStateToProp, {fetchUser})(UserHeader);

// export default connect(mapStateToProp, {fetchUser})(UserHeader);
export default connect(mapStateToProp)(UserHeader);